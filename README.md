# Rust Upkeep Monitor

## What is it
A free-to-use open-source tool for players of the game Rust.

<a href="docs/Dashboard.png" target="_blank"><img src="docs/Dashboard.png" alt="MarineGEO circle logo" style="height: 30%; width: 70%;"/></a>

## Audience 
Rust players who want to monitor the remaining upkeep time of multiple in-game bases, before they start to decay.

## Features
- **Upkeep Tracking**: Enter and track your base's upkeep details manually.
- **Platform Support**: Available as a website and as a Windows application.

## Technical Details
This project is developed using C# in a Blazor Hybrid environment.

- **Web Interface**: Implemented with Blazor WebAssembly, accessible at [RustUpkeepMonitor](https://rust.bf2.tv).
- **Windows Application**: Available as a Windows Forms Application. Manual build is required using the dotnet CLI or Visual Studio.

## Getting Started
- For the web version, simply visit https://rust.bf2.tv
- To use the Windows application, a manual build is necessary using the dotnet CLI or Visual Studio.

## Contribution
As this is an open-source project, any feedback, bug reports, or suggestions for improvements are greatly appreciated.

## License
RustUpkeepMonitor is open-source and is available under the MIT License.
