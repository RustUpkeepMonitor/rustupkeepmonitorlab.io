﻿using System.Text;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Components;
using System.Text.Json.Serialization;
using Blazored.SessionStorage;

namespace RustUpkeepMonitor.Blazor.Services;

public class DiscordAuthService(
    NavigationManager navigationManager,
    HttpClient httpClient,
    ISessionStorageService sessionStorage)
{
    private const string ClientId = "1191442932805410857";

    public async Task SignInAsync(string currentUrl)
    {
        if (string.IsNullOrEmpty(ClientId))
            throw new InvalidDataException(
                "Discord ClientId not properly set up in appsettings.json (via Discord Developer Portal)");

        var codeVerifier = CreateCodeVerifier();
        await sessionStorage.SetItemAsync("codeVerifier", codeVerifier);
        var redirectUri = Uri.EscapeDataString(currentUrl);
        Console.WriteLine(redirectUri);
        var state = Guid.NewGuid().ToString("N");
        var codeChallenge = CreateCodeChallenge(codeVerifier);

        var discordAuthUrl =
            $"https://discord.com/api/oauth2/authorize?client_id={ClientId}&redirect_uri={redirectUri}&response_type=code&scope=identify&state={state}&code_challenge_method=S256&code_challenge={codeChallenge}";
        navigationManager.NavigateTo(discordAuthUrl);
    }

    public async Task<string> ExchangeCodeForTokenAsync(string code, string currentUrl)
    {
        var codeVerifier = await sessionStorage.GetItemAsync<string>("codeVerifier");

        var tokenRequest = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("client_id", ClientId),
            new KeyValuePair<string, string>("grant_type", "authorization_code"),
            new KeyValuePair<string, string>("code", code),
            new KeyValuePair<string, string>("redirect_uri", currentUrl),
            new KeyValuePair<string, string>("code_verifier", codeVerifier)
        });

        HttpResponseMessage? response = null;

        try
        {
            response = await httpClient.PostAsync("https://discord.com/api/oauth2/token", tokenRequest);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var tokenResponse = System.Text.Json.JsonSerializer.Deserialize<DiscordTokenResponse>(content);
            return tokenResponse?.AccessToken ?? throw new InvalidDataException("No access token received");
        }
        catch (HttpRequestException ex)
        {
            var responseContent =
                response != null ? await response.Content.ReadAsStringAsync() : "No response received";

            throw new InvalidOperationException(
                $"Error in ExchangeCodeForTokenAsync: {ex.Message}, Response Content: {responseContent}", ex);
        }
    }

    private string CreateCodeVerifier()
    {
        var bytes = new byte[32];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(bytes);
        return Convert.ToBase64String(bytes).TrimEnd('=').Replace('+', '-').Replace('/', '_');
    }

    private string CreateCodeChallenge(string codeVerifier)
    {
        using var sha256 = SHA256.Create();
        var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(codeVerifier));
        return Convert.ToBase64String(hash).TrimEnd('=').Replace('+', '-').Replace('/', '_');
    }

    private class DiscordTokenResponse
    {
        [JsonPropertyName("access_token")] public string? AccessToken { get; init; }
    }
}