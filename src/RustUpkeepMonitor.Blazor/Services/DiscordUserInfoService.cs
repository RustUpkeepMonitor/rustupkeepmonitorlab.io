﻿using RustUpkeepMonitor.Blazor.Models;

namespace RustUpkeepMonitor.Blazor.Services;

public class DiscordUserInfoService(HttpClient httpClient)
{
    public async Task<DiscordUserInfo> FetchUserInfoAsync(string accessToken)
    {
        var requestMessage = new HttpRequestMessage(HttpMethod.Get, "https://discord.com/api/users/@me");
        requestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);

        var response = await httpClient.SendAsync(requestMessage);
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadAsStringAsync();

        var user = System.Text.Json.JsonSerializer.Deserialize<DiscordUserInfo>(content);
        return user ?? throw new InvalidDataException("Failed to read Discord user information");
    }
}