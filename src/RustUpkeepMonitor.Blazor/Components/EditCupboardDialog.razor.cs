﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using MudBlazor;
using RustUpkeepMonitor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RustUpkeepMonitor.Blazor.Components
{
    public partial class EditCupboardDialog
    {
        public static string EditAction => "Edit";
        public static string AddAction => "Add";

        public static DialogOptions Options = new DialogOptions()
        {
            CloseOnEscapeKey = true,
            Position = DialogPosition.TopCenter,
            NoHeader = true,
        };

        [CascadingParameter] MudDialogInstance MudDialog { get; set; } = default!;

        [Parameter] public Cupboard Cupboard { get; set; } = default!;

        private async Task OnKeyDown(KeyboardEventArgs e)
        {
            if (e.Code == "Enter" || e.Code == "NumpadEnter")
            {
                // For some reason, only in Web, the dialog acts weird if I don't do this (I think Enter gets triggerd again and dialog re-opens)
                await Task.Delay(100);
                OnOk();
            }
        }

        private void OnDelete()
        {
            Cupboard.ShouldDelete = true;
            MudDialog.Close(DialogResult.Ok(Cupboard));
        }

        private void OnOk()
        {
            Cupboard.LastUserUpdate = DateTime.Now;
            MudDialog.Close(DialogResult.Ok(Cupboard));
        }
    }
}
