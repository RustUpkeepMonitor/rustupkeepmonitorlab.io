using Blazored.SessionStorage;
using Microsoft.Extensions.DependencyInjection;
using MudBlazor.Services;
using RustUpkeepMonitor.Blazor.Models;
using RustUpkeepMonitor.Blazor.Services;
using RustUpkeepMonitor.Core.Services;
using Blazored.LocalStorage;
using RustUpkeepMonitor.Communication.Services;

namespace RustUpkeepMonitor.Blazor
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServicesFromBlazorProject(this IServiceCollection services)
        {
            services.AddBlazoredSessionStorage();
            services.AddBlazoredLocalStorage();

            services.AddMudServices();
            services.AddMudBlazorDialog();

            services.AddScoped<DiscordAuthService>();
            services.AddScoped<DiscordUserInfoService>();
            services.AddSingleton<TimerService>();
            services.AddSingleton<UiPersistence>();
            services.AddScoped<SettingsService>();

            return services;
        }
    }
}
