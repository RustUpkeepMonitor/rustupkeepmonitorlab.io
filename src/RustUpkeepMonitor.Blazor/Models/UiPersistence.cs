﻿using System.Text.Json;
using Microsoft.JSInterop;
using RustUpkeepMonitor.Blazor.Pages;
using RustUpkeepMonitor.Core.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Runtime.CompilerServices;

namespace RustUpkeepMonitor.Blazor.Models;

public class UiPersistence
{
    public Action<List<Cupboard>>? OnSaveRequested { get; set; }

    public List<Cupboard> Cupboards { get; set; } = new List<Cupboard>();

    public void RemoveCupboard(Cupboard cupboard)
    {
        Cupboards.RemoveAll(x => x.Guid == cupboard.Guid);
        SaveChanges();
    }

    public void AddCupboard(Cupboard cupboard)
    {
        Cupboards.Add(cupboard);
        SaveChanges();
    }

    public void SaveChanges()
    {
        OnSaveRequested.Invoke(Cupboards);
    }
}
