﻿namespace RustUpkeepMonitor.Blazor.Models
{
    public class UserModel
    {
        public const string StorageKey = "User01";
        
        public string Token { get; set; }
        public DiscordUserInfo Info { get; set; }
    }
}