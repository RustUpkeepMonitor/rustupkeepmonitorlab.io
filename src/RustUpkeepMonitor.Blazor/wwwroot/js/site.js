﻿function setupVisibilityChange(callback) {
    function handleVisibilityChange() {
        if (!document.hidden) {
            callback.invokeMethodAsync("OnTabActivated");
        }
    }

    document.addEventListener("visibilitychange", handleVisibilityChange);

    return {
        dispose: function () {
            document.removeEventListener("visibilitychange", handleVisibilityChange);
        }
    };
}
