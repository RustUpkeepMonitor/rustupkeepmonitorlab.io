﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RustUpkeepMonitor.Core.Extensions;

public static class DateTimeExtensions
{
    public static string GetTimeAgo(this DateTime dateTime, bool useShortVersion = false)
    {
        var timeSpan = DateTime.Now - dateTime;
        string minuteUnit = useShortVersion ? "min" : "minutes";
        string hourUnit = useShortVersion ? "hrs" : "hours";
        string hourSingleUnit = useShortVersion ? "hr" : "hour";
        string dayUnit = useShortVersion ? "d" : "days";
        string daySingleUnit = useShortVersion ? "d" : "day";

        if (timeSpan.TotalMinutes < 1)
            return "now";
        else if (timeSpan.TotalMinutes < 2)
            return $"1 {hourSingleUnit} ago";
        else if (timeSpan.TotalMinutes < 60)
            return $"{Math.Floor(timeSpan.TotalMinutes)} {minuteUnit} ago";
        else if (timeSpan.TotalHours < 2)
            return $"1 {hourSingleUnit} ago";
        else if (timeSpan.TotalHours < 24)
            return $"{Math.Floor(timeSpan.TotalHours)} {hourUnit} ago";
        else if (timeSpan.TotalDays < 2)
            return $"1 {daySingleUnit} ago";
        else if (timeSpan.TotalDays < 100)
            return $"{Math.Floor(timeSpan.TotalDays)} {dayUnit} ago";
        else
            return "long ago";
    }

}
