﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RustUpkeepMonitor.Core.Services;

public class TimerService
{
    private readonly PeriodicTimer timer;
    private readonly List<Action> subscribers = [];

    public TimerService()
    {
        timer = new PeriodicTimer(TimeSpan.FromMinutes(1));
        RunTimer();
    }

    private async void RunTimer()
    {
        while (await timer.WaitForNextTickAsync())
        {
            NotifyAllSubscribers();
        }
    }

    public void Subscribe(Action callback)
    {
        subscribers.Add(callback);
    }

    public void Unsubscribe(Action callback)
    {
        subscribers.Remove(callback);
    }

    public void NotifyAllSubscribers()
    {
        foreach (var subscriber in subscribers)
        {
            subscriber?.Invoke();
        }
    }
}