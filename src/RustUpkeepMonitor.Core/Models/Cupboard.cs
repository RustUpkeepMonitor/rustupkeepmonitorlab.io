﻿namespace RustUpkeepMonitor.Core.Models
{
    public class Cupboard
    {
        public Guid Guid { get; set; } = Guid.NewGuid();
        public string? Label { get; set; } = "Base";
        public int? LastUserDaysUntilDecayInput { get; set; } = 0;
        public int? LastUserHoursUntilDecayInput { get; set; } = 24;
        public int? LastUserMinutesUntilDecayInput { get; set; } = 0;
        public DateTime? WhenDecayBegins
        {
            get
            {
                if (LastUserUpdate != null)
                {
                    int days = LastUserDaysUntilDecayInput ?? 0;
                    int hours = LastUserHoursUntilDecayInput ?? 0;
                    int minutes = LastUserMinutesUntilDecayInput ?? 0;

                    return LastUserUpdate.Value + TimeSpan.FromDays(days) + TimeSpan.FromHours(hours) + TimeSpan.FromMinutes(minutes);
                }
                return null;
            }
        }

        public DateTime? LastUserUpdate { get; set; } = DateTime.Now;
        public bool ShouldDelete { get; set; } = false;
    }
}