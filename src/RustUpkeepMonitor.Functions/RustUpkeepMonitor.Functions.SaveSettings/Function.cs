using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.Core;
using JetBrains.Annotations;
using RustUpkeepMonitor.Communication.Models;
using RustUpkeepMonitor.Communication.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace RustUpkeepMonitor.Functions.SaveSettings;

public class Function
{
    private static readonly AmazonDynamoDBClient Client = new();
    private static readonly DynamoDBContext Context = new(Client);

    [UsedImplicitly]
    public async Task<string> FunctionHandler(SaveSettingsRequest request, ILambdaContext context)
    {
        if (!TokenValidator.Validate(request.Token))
            throw new InvalidOperationException("Not allowed");

        LambdaLogger.Log("Saving settings...");
        await SaveUserSettings(request.UserSettings);
        LambdaLogger.Log("Settings saved!");

        return "Done";
    }

    private async Task SaveUserSettings(UserSettings settings)
    {
        var existingUserSettings = await Context.LoadAsync<UserSettings>(settings.PartitionKey);
        if (existingUserSettings != null)
        {
            existingUserSettings.Cupboards = settings.Cupboards;
            await Save(existingUserSettings);
        }
        else
        {
            await Save(settings);
        }
    }

    private static async Task Save(UserSettings existingUserSettings)
    {
        var batch = Context.CreateBatchWrite<UserSettings>();
        batch.AddPutItem(existingUserSettings);
        await batch.ExecuteAsync();
    }
}