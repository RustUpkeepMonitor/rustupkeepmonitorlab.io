using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.Core;
using JetBrains.Annotations;
using RustUpkeepMonitor.Communication.Models;
using RustUpkeepMonitor.Communication.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace RustUpkeepMonitor.Functions.LoadSettings;

public class Function
{
    private static readonly AmazonDynamoDBClient Client = new();
    private static readonly DynamoDBContext Context = new(Client);

    [UsedImplicitly]
    public async Task<UserSettings> FunctionHandler(LoadSettingsRequest request, ILambdaContext context)
    {
        if (!TokenValidator.Validate(request.Token))
            throw new InvalidOperationException("Not allowed");

        LambdaLogger.Log("Loading settings...");
        var result = await LoadUserSettings(request.UserId, request.LocalTimeOfRequest);
        LambdaLogger.Log("Settings loaded!");

        return result;
    }

    private async Task<UserSettings> LoadUserSettings(string userId, DateTime localTimeOfRequest)
    {
        UserSettings? result;

        try
        {
            result = await Context.LoadAsync<UserSettings>(userId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return result ?? CreateInitialSettings(userId, localTimeOfRequest);
    }

    private static UserSettings CreateInitialSettings(string userId, DateTime localTimeOfRequest)
    {
        var whenDecayBegins = localTimeOfRequest.AddHours(24);
        return new UserSettings()
        {
            PartitionKey = userId,
            Cupboards = new List<StoredCupboard>
            {
                new()
                {
                    Guid = Guid.NewGuid(),
                    Label = "Initial Base",
                    WhenDecayBegins = whenDecayBegins,
                    WhenLastUserUpdate = localTimeOfRequest,
                },
            },
        };
    }
}