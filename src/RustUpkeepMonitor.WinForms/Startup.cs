using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RustUpkeepMonitor.Blazor;

namespace RustUpkeepMonitor;

public static class Startup
{
    public static IServiceProvider? Services { get; private set; }

    public static void Init()
    {
        var host = Host.CreateDefaultBuilder()
                       .ConfigureServices(WireupServices)
                       .Build();
        Services = host.Services;
    }

    private static void WireupServices(IServiceCollection services)
    {
        services.AddScoped<HttpClient>();
        services.AddWindowsFormsBlazorWebView();
        services.AddServicesFromBlazorProject();

#if DEBUG
        services.AddBlazorWebViewDeveloperTools();
#endif
    }
}
