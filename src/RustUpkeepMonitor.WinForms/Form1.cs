using Microsoft.AspNetCore.Components.WebView.WindowsForms;
using RustUpkeepMonitor.Infrastructure;

namespace RustUpkeepMonitor;

public partial class Form1 : Form
{
    private readonly BlazorWebView _blazorWebView;

    public Form1()
    {
        InitializeComponent();

        _blazorWebView = new BlazorWebView()
        {
            Dock = DockStyle.Fill,
            HostPage = "wwwroot/index.html",
            Services = Startup.Services!,
            StartPath = "/"
        };

        _blazorWebView.RootComponents.Add<Main>("#app");
        Controls.Add(_blazorWebView);
    }

    protected override void OnHandleCreated(EventArgs e) => DarkAppMode.Enable(Handle, this, _blazorWebView);
}
