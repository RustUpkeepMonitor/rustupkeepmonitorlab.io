﻿using Microsoft.AspNetCore.Components.WebView.WindowsForms;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;

namespace RustUpkeepMonitor.Infrastructure;

/// <summary>
/// Black magic to make the form title bar go dark, from https://stackoverflow.com/a/64927217
/// 
/// To be invoked like this, from a form:
///     <code>
///         protected override void OnHandleCreated(EventArgs e) => DarkAppMode.Enable(Handle, this, _blazorWebView);
///     </code>
/// </summary>
public static partial class DarkAppMode
{
    private static readonly Color _color = Color.Black;

    [LibraryImport("DwmApi")]
    private static partial int DwmSetWindowAttribute(IntPtr hwnd, int attr, [In] int[] attrValue, int attrSize);

    public static void Enable(IntPtr intPtr, Form form, BlazorWebView blazorWebView)
    {
        form.BackColor = _color;
        blazorWebView.WebView.DefaultBackgroundColor = _color;

        if (DwmSetWindowAttribute(intPtr, 19, [1], 4) != 0)
            DwmSetWindowAttribute(intPtr, 20, [1], 4);
    }
}