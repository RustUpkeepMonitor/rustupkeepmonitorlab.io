﻿namespace RustUpkeepMonitor.Communication.Models;

public class StoredCupboard
{
    public Guid Guid { get; set; }
    public string Label { get; set; }
    public DateTime WhenDecayBegins { get; set; }
    public DateTime WhenLastUserUpdate { get; set; }
}