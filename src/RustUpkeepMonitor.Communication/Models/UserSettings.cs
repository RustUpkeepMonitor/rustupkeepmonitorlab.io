﻿using Amazon.DynamoDBv2.DataModel;

namespace RustUpkeepMonitor.Communication.Models
{
    [DynamoDBTable("UserSettings")]
    public class UserSettings
    {
        [DynamoDBHashKey] // AWS Partition key
        // ReSharper disable once InconsistentNaming
        public string PartitionKey { get; set; }

        public List<StoredCupboard> Cupboards { get; set; }
    }
}