﻿namespace RustUpkeepMonitor.Communication.Models
{
    public abstract class TokenizedPayload
    {
        public string Token { get; set; }
    }
}