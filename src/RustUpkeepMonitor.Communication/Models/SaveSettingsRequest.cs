﻿namespace RustUpkeepMonitor.Communication.Models;

public class SaveSettingsRequest : TokenizedPayload
{
    public UserSettings UserSettings { get; }

    public SaveSettingsRequest(UserSettings userSettings, string token)
    {
        UserSettings = userSettings;
        Token = token;
    }
}