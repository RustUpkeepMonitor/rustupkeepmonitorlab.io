﻿using JetBrains.Annotations;

namespace RustUpkeepMonitor.Communication.Models
{
    [UsedImplicitly]
    public class LoadSettingsRequest : TokenizedPayload
    {
        public string UserId { get; }

        public DateTime LocalTimeOfRequest { get; }

        public LoadSettingsRequest(string userId, DateTime localTimeOfRequest, string token)
        {
            UserId = userId;
            LocalTimeOfRequest = localTimeOfRequest;
            Token = token;
        }
    }
}