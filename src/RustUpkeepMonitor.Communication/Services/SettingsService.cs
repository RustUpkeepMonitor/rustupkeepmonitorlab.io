﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using RustUpkeepMonitor.Communication.Models;

namespace RustUpkeepMonitor.Communication.Services
{
    public class SettingsService
    {
        private const string LoadSettingsEndpointUrl =
            "https://uzwmq0p0aa.execute-api.eu-central-1.amazonaws.com/myMainStage/RustUpkeepMonitorFunctionsLoadSettings";

        private const string SaveSettingsEndpointUrl =
            "https://uzwmq0p0aa.execute-api.eu-central-1.amazonaws.com/myMainStage/RustUpkeepMonitorFunctionsSaveSettings";

        private readonly HttpClient _httpClient;

        public SettingsService(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<string> SaveUserSettingsAsync(UserSettings settings)
        {
            var request = new SaveSettingsRequest(settings, Guid.NewGuid().ToString());
            var json = JsonSerializer.Serialize(request);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(SaveSettingsEndpointUrl, content);

            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<UserSettings> LoadUserSettingsAsync(LoadSettingsRequest request)
        {
            var json = JsonSerializer.Serialize(request);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(LoadSettingsEndpointUrl, content);

            response.EnsureSuccessStatusCode();
            var responseJson = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(responseJson))
                throw new InvalidOperationException("Failed to receive response");

            var responseParsed = JsonSerializer.Deserialize<UserSettings>(responseJson);
            if (responseParsed == null)
                throw new InvalidOperationException($"Failed to parse response JSON:\n{responseJson}");

            return responseParsed;
        }
    }
}